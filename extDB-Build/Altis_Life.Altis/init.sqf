StartProgress = false;
enableSaving[false,false];

life_versionInfo = "Altis Life RPG v3.1.5";
[] execVM "briefing.sqf"; //Load Briefing
[] execVM "KRON_Strings.sqf";
[] execVM "scripts\makeAtm.sqf"; //SP's atm magic
[] execVM "scripts\addPetrolStations.sqf"; //SP's petrolstation magic

if (isServer) then {call compile preprocessFile "scripts\Map\locationlist.sqf";};

StartProgress = true;