/// Front + Towers
_pos = [9506.182617,15216.458008,-0.274384];
_object = createVehicle ["Land_Cargo_Tower_V1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 160;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9559.486328,15233.638672,-0.187759];
_object = createVehicle ["Land_Cargo_Patrol_V1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 300;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9556.842773,15292.967773,-0.0704956];
_object = createVehicle ["Land_Cargo_Patrol_V1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 220;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9512.78125,15296.258789,-0.00323486];
_object = createVehicle ["Land_Cargo_Patrol_V1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 190;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9480.211914,15293.900391,-0.900055];
_object = createVehicle ["Land_Cargo_Tower_V1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 9.53674e-006;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9552.594727,15268.00390625,0.391006];
_object = createVehicle ["Land_Cargo_HQ_V1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 358;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

/// Landing lights
_pos = [9531.90625,15236.69043,0.0235748];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9537.110352,15226.764648,0.000442505];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9547.0664063,15232.00488281,0.000152588];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9541.805664,15241.907227,0.00854492];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9536.306641,15232.712891,-0.00378418];
_object = createVehicle ["Land_Flush_Light_red_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9540.139648,15238.862305,0];
_object = createVehicle ["Land_Flush_Light_red_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9543.533203,15232.370117,0];
_object = createVehicle ["Land_Flush_Light_red_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9515.759766,15266.736328,0];
_object = createVehicle ["Land_Flush_Light_red_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9513.329102,15273.665039,0];
_object = createVehicle ["Land_Flush_Light_red_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9508.556641,15268.0449219,0];
_object = createVehicle ["Land_Flush_Light_red_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9505.0439453,15268.470703,0];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9512.366211,15276.97168,0];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9520.860352,15269.773438,0];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9513.59082,15261.248047,0];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9491.84375,15284.564453,-0.936447];
_object = createVehicle ["Land_LampStreet_small_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 40;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9537.580078,15293.172852,-1.89839];
_object = createVehicle ["Land_LampStreet_small_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9544.946289,15224.702148,-3];
_object = createVehicle ["Land_LampStreet_small_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9537.078125,15297.330078,-0.967957];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 89.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9523.324219,15297.535156,-0.271454];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 80;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9561.696289,15274.585938,-1];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 176;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;

_pos = [9562.989258,15250.992188,-1];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 180;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;

_pos = [9552.839844,15226.869141,-0.99733];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 241;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9533.791016,15218.139648,-0.981949];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 241;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9510.100586,15208.865234,-1];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 243;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9495.270508,15211.0742188,-0.487152];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 330;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9486.239258,15228.620117,-0.800049];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 330;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9478.241211,15243.732422,-0.600037];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 335;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9473.104492,15260.647461,-0.500031];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 355;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9472.09375,15279.524414,0];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9473.314453,15304.0751953,0];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 9;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [9484.970703,15304.675781,-0.437515];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 99.9999;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;

_pos = [9503.946289,15300.946289,-0.200012];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 99.9999;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;

_pos = [9045.546875,16023.928711,0];
_object = createVehicle ["Land_LampShabby_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 180;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;

/// Walls
_pos = [9523.642578,15297.552734,0.524872];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 191;
_object setPosATL _pos;
[_object, 1, -1.42762e-006] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9519.735352,15298.353516,0.626633];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 192;
_object setPosATL _pos;
[_object, 1, -2.96198e-006] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9515.836914,15299.0800781,0.591751];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 189;
_object setPosATL _pos;
[_object, 1, -1.02735e-006] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9511.907227,15299.706055,0.525467];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 189;
_object setPosATL _pos;
[_object, 1, -3.37559e-006] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9507.976563,15300.455078,0.565918];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 192;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9504.078125,15301.269531,0.578888];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 192;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9500.181641,15302.124023,0.54686];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 192;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9496.290039,15302.897461,0.508987];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 190;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9492.359375,15303.546875,0.513458];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 188;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9488.421875,15304.166992,0.501846];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 189;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9484.484375,15304.791992,0.446503];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 188;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9480.543945,15305.390625,0.476028];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 188;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9476.576172,15305.939453,0.485092];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 187;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9473.795898,15305.954102,0.644211];
_object = createVehicle ["Land_Mil_WallBig_Corner_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 95.9998;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9473.201172,15303.228516,0.597656];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 97;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9472.732422,15299.263672,0.465714];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 95.9998;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9472.317383,15295.294922,0.419601];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 95.9999;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9471.885742,15291.335938,0.534927];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 95.9999;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9471.59375,15287.374023,0.453812];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 90.9998;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9471.573242,15283.4375,0.479553];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 88.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9471.642578,15279.475586,0.567719];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 88.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9471.706055,15275.516602,0.524689];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 88.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9471.84082,15271.536133,0.566544];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 86.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9472.124023,15267.566406,0.58577];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 83.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9472.519531,15263.598633,0.771545];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 82.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9473.0800781,15259.65332,0.4198];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 80.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9473.776367,15255.716797,0.272095];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 79;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9474.751953,15251.902344,0.500488];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 71.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9476.124023,15248.207031,0.48967];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 67;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9477.741211,15244.566406,0.47821];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 63.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9479.479492,15240.992188,0.631897];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 62.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9481.31543,15237.460938,0.342621];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 62;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9483.199219,15233.94043,0.329117];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 62;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9485.03125,15230.383789,0.32019];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 64;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9486.854492,15226.825195,0.303467];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 62;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9488.65918,15223.28125,0.369461];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 64;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9490.40332,15219.682617,0.310379];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 63.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9492.197266,15216.12793,0.409729];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 62;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9494.171875,15212.677734,0.429047];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 58;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9496.256836,15209.249023,0.469818];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 59;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9498.447266,15206.000976563,0.523621];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 52.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9500.386719,15204.249023,0.386932];
_object = createVehicle ["Land_Mil_WallBig_Corner_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 309;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9502.980469,15205.40918,-0.012085];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 340;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9506.688477,15206.822266,0.390366];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 339;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9510.397461,15208.233398,0.252625];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 339;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9514.0996094,15209.671875,0.558762];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 338;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9517.790039,15211.142578,0.513306];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 338;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9521.462891,15212.649414,0.434158];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 337;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9525.118164,15214.21875,0.274719];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9528.790039,15215.772461,-0.0877533];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 337;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9532.488281,15217.311523,0.674835];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9536.136719,15218.908203,0.594345];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9539.788086,15220.506836,0.598511];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9543.433594,15222.116211,0.597275];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9547.0810547,15223.723633,0.596985];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9550.732422,15225.333008,0.551727];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9554.370117,15226.955078,0.506104];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9557.989258,15228.566406,0.332596];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9561.599609,15230.192383,0.246353];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 336;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9563.975586,15231.575195,-0.0623627];
_object = createVehicle ["Land_Mil_WallBig_Corner_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 244;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9563.749023,15234.362305,0.681152];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9563.53418,15238.34668,0.588684];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 266;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9563.317383,15242.335938,0.523209];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9563.12793,15246.285156,0.49707];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9562.995117,15250.280273,0.571594];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 268;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9562.907227,15254.271484,0.545059];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 268;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9562.750977,15258.256836,0.442673];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9562.558594,15262.254883,0.51358];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 266;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9562.305664,15266.239258,0.480621];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 266;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9562.0771484,15270.234375,0.615021];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9561.898438,15274.164063,0.529434];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9561.742188,15278.148438,0.563828];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 268;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9561.594727,15282.104492,0.606735];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9561.416992,15286.0566406,0.500626];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9561.234375,15290.0390625,0.499908];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9561.0498047,15294.0205078,-0.0613251];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9560.670898,15296.787109,-0.00793457];
_object = createVehicle ["Land_Mil_WallBig_Corner_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 180;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9557.918945,15297.173828,-0.0619659];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 183;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9553.946289,15297.423828,0.702515];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 184;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9550,15297.603516,0.60257];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 181;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9546.0322266,15297.681641,0.548752];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 180;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9542.0556641,15297.643555,0.570099];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 178;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9538.151367,15297.401367,0.000579834];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 174;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9525.542969,15293.749023,-0.384155];
_object = createVehicle ["Land_City_8m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 87.9999;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9525.730469,15288.501953,-0.402374];
_object = createVehicle ["Land_City_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 267.9;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9525.859375,15284.983398,-0.429245];
_object = createVehicle ["Land_City_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 268.3;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [9525.883789,15283.0244141,-0.322189];
_object = createVehicle ["Land_City_Pillar_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;
