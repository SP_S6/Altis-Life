﻿_pos = [10229.0644531,15887.845703,-0.0106354];
_object = createVehicle ["Land_HBarrierWall_corner_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 104.966;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10233.0664063,15909.875977,0.0214462];
_object = createVehicle ["Land_Wall_IndCnc_4_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 99.0002;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10233.59375,15915.71582,0.0703049];
_object = createVehicle ["Land_Wall_IndCnc_4_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 91.9998;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10197.572266,15931.75,-0.0166473];
_object = createVehicle ["Land_Wall_IndCnc_4_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 11;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10194.366211,15932.24707,-0.0147858];
_object = createVehicle ["Land_City_Pillar_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10193.888672,15932.443359,0.187424];
_object = createVehicle ["Land_City_Pillar_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10193.888672,15932.443359,0.187424];
_object = createVehicle ["Land_City_Pillar_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10156.972656,15928.126953,-0.00170135];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 325;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10155.0527344,15929.708008,0.297997];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 190.044;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10151.112305,15930.413086,0.330475];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 190.23;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10147.167969,15931.131836,0.280495];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 190.688;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10143.233398,15931.87207,0.141502];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 190.612;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10139.301758,15932.608398,0];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 190.6;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10135.367188,15933.34082,0.000671387];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 190.49;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10131.424805,15934.0410156,-0.0144348];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 189.9;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10127.477539,15934.730469,0.0086441];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 190.121;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10124.696289,15934.927734,-0.0904388];
_object = createVehicle ["Land_Mil_WallBig_Corner_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 100;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10124.197266,15859.863281,-0.0495911];
_object = createVehicle ["Land_HBarrierBig_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 79.051;
_object setPosATL _pos;
[_object, -3.91224, -13.0892] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10118.851563,15927.235352,-0.100021];
_object = createVehicle ["Land_Mil_WallBig_Corner_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 66.9999;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10118.288086,15924.50293,0.000511169];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 107.699;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10117.0615234,15920.693359,0.00280762];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 107.953;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10115.849609,15918.125977,-0.0472412];
_object = createVehicle ["Land_Mil_WallBig_Corner_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 62.5999;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10115.375977,15915.380859,-0.012352];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 107.719;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10114.516602,15907.477539,-0.00418091];
_object = createVehicle ["Land_BarGate_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 109.3;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10121.194336,15914.572266,0.191536];
_object = createVehicle ["Land_CncBarrierMedium4_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 18.0638;
_object setPosATL _pos;
[_object, -0.659939, -1.1793] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10128.678711,15851.0771484,0.08564];
_object = createVehicle ["Land_HBarrierBig_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 226.39;
_object setPosATL _pos;
[_object, 13.9467, 9.69541] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10147.785156,15910.0703125,-0.00266266];
_object = createVehicle ["Land_Cargo_House_V1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 106.699;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10155.154297,15908.519531,0];
_object = createVehicle ["Land_LampHalogen_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 228.949;
_object setPosATL _pos;

_pos = [10154.242188,15856.806641,-0.231819];
_object = createVehicle ["Land_HBarrier_Big_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 279.614;
_object setPosATL _pos;
[_object, -1.11483, 8.35205] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10152.863281,15842.111328,0.0967026];
_object = createVehicle ["Land_HBarrier_Big_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 271.723;
_object setPosATL _pos;
[_object, 2.76898, 16.6426] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10150.200195,15900.570313,0.24865];
_object = createVehicle ["Land_BarGate_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 285.492;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10138.832031,15889.507813,-0.309593];
_object = createVehicle ["Land_Cargo_Tower_V1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 18.984;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10087.825195,15913.15625,0.457092];
_object = createVehicle ["Land_LampStreet_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 22;
_object setPosATL _pos;

_pos = [10111.269531,15902.421875,-0.063942];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 107.299;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10110.0576172,15898.607422,0.00978851];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 108;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10108.81543,15894.799805,0.0340042];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 108.3;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10107.591797,15890.988281,0.109589];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 107.85;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10108.163086,15884.672852,-0.700005];
_object = createVehicle ["Land_HBarrier_Big_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 70;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10109.748047,15880.783203,-0.452042];
_object = createVehicle ["Land_HBarrierBig_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 249.9;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10118.163086,15872.325195,-0.605225];
_object = createVehicle ["Land_HBarrierBig_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 40.4014;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10153.00585938,15848.479492,-0.243965];
_object = createVehicle ["Land_HBarrier_Big_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 279.839;
_object setPosATL _pos;
[_object, -0.972834, 4.70533] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10154.833008,15864.888672,-0.100685];
_object = createVehicle ["Land_HBarrier_Big_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 272.642;
_object setPosATL _pos;
[_object, -3.60184, 13.3304] call BIS_fnc_setPitchBank;
_object allowDamage false;


_pos = [10159.0800781,15879.0566406,-0.013649];
_object = createVehicle ["Land_HBarrier_Big_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 283;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10159.120117,15885.179688,-0.0395508];
_object = createVehicle ["Land_HBarrier_Big_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 79.951;
_object setPosATL _pos;
[_object, 1.33467, -2.09883] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10153.286133,15891.313477,-0.0948105];
_object = createVehicle ["Land_HBarrierBig_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 28.4623;
_object setPosATL _pos;
[_object, -0.774476, -2.41749] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10152.506836,15884.493164,0.0412827];
_object = createVehicle ["Land_Cargo_Patrol_V1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 287.549;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10146.198242,15885.1875,-0.00338745];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 286.972;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10147.357422,15889.0214844,0.0378571];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 286.796;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10148.542969,15892.84375,0.00133514];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 287.666;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10152.822266,15905.835938,0.106056];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 287.869;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10154.0585938,15909.649414,-0.000198364];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 287.939;
_object setPosATL _pos;
_object allowDamage false;


_pos = [10190.59668,15932.49707,-0.00248718];
_object = createVehicle ["Land_Wall_IndCnc_4_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 180;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10123.108398,15932.625,0.130516];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 126.699;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10120.723633,15929.405273,0.00442505];
_object = createVehicle ["Land_Mil_WallBig_4m_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 126.299;
_object setPosATL _pos;
_object allowDamage false;

_pos = [10157.650391,15928.981445,0.0940094];
_object = createVehicle ["Land_Mil_WallBig_Corner_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 193.8;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10128.303711,15912.355469,0.120178];
_object = createVehicle ["Land_CncBarrierMedium4_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 18.2165;
_object setPosATL _pos;
[_object, -0.490506, -1.93317] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10121.194336,15914.572266,0.117798];
_object = createVehicle ["Land_CncBarrierMedium4_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 18.0638;
_object setPosATL _pos;
[_object, -0.659939, -1.1804] call BIS_fnc_setPitchBank;
_object allowDamage false;

_pos = [10128.810547,15922.0498047,3.8147e-005];
_object = createVehicle ["Land_HelipadSquare_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 17.0244;
_object setPosATL _pos;
[_object, -0.861488, -1.62158] call BIS_fnc_setPitchBank;
_object allowDamage false;


_pos = [10129.866211,15925.523438,-0.00871277];
_object = createVehicle ["Land_Flush_Light_red_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0.0111915;
_object setPosATL _pos;
[_object, -1.68013, -0.384627] call BIS_fnc_setPitchBank;

_pos = [10124.519531,15920.543945,-0.00944519];
_object = createVehicle ["Land_Flush_Light_red_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0.0109729;
_object setPosATL _pos;
[_object, -0.916485, -0.687722] call BIS_fnc_setPitchBank;

_pos = [10131.517578,15918.384766,-0.00852966];
_object = createVehicle ["Land_Flush_Light_red_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0.0294144;
_object setPosATL _pos;
[_object, -1.2978, -1.30381] call BIS_fnc_setPitchBank;

_pos = [10132.525391,15915.0517578,-0.00837708];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0.0535933;
_object setPosATL _pos;
[_object, -1.29625, -2.36807] call BIS_fnc_setPitchBank;

_pos = [10121.806641,15918.351563,-0.0065918];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0.0109729;
_object setPosATL _pos;
[_object, -0.916485, -0.688251] call BIS_fnc_setPitchBank;

_pos = [10125.107422,15929.0371094,-0.00845337];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0.0173113;
_object setPosATL _pos;
[_object, -0.763553, -1.30031] call BIS_fnc_setPitchBank;

_pos = [10135.811523,15925.761719,-0.00827789];
_object = createVehicle ["Land_Flush_Light_green_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0.0760237;
_object setPosATL _pos;
[_object, -1.67676, -2.61119] call BIS_fnc_setPitchBank;

_pos = [10098.792969,15903.418945,0];
_object = createVehicle ["Land_Flush_Light_yellow_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;
[_object, 0, 0] call BIS_fnc_setPitchBank;

_pos = [10105.757813,15901.209961,-0.00535583];
_object = createVehicle ["Land_Flush_Light_yellow_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 359.997;
_object setPosATL _pos;
[_object, -2.21434, 0.0780301] call BIS_fnc_setPitchBank;

_pos = [10100.375977,15896.257813,-0.00717926];
_object = createVehicle ["Land_Flush_Light_yellow_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0.0213821;
_object setPosATL _pos;
[_object, -3.20506, -0.384389] call BIS_fnc_setPitchBank;

_pos = [10118.223633,15893.629883,-0.0839691];
_object = createVehicle ["CamoNet_INDP_big_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 212;
_object setPosATL _pos;


_pos = [10117.664063,15894.310547,-0.0113144];
_object = createVehicle ["Campfire_burning_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0.0327749;
_object setPosATL _pos;
[_object, -1.75588, -1.09582] call BIS_fnc_setPitchBank;


_pos = [10117.357422,15890.676758,0.0403366];
_object = createVehicle ["Land_Sleeping_bag_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 200.153;
_object setPosATL _pos;
[_object, 4.08559, 2.1519] call BIS_fnc_setPitchBank;


_pos = [10115.84668,15891.256836,0.0380249];
_object = createVehicle ["Land_Sleeping_bag_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 207.174;
_object setPosATL _pos;
[_object, 3.79313, 2.63516] call BIS_fnc_setPitchBank;


_pos = [10114.137695,15891.864258,0.0370789];
_object = createVehicle ["Land_Sleeping_bag_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 209.178;
_object setPosATL _pos;
[_object, 3.69899, 2.76611] call BIS_fnc_setPitchBank;


_pos = [10119.308594,15890.174805,0.0750961];
_object = createVehicle ["Land_Sleeping_bag_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 169.781;
_object setPosATL _pos;
[_object, 4.09742, -3.08145] call BIS_fnc_setPitchBank;


_pos = [10112.791016,15901.396484,-0.000427246];
_object = createVehicle ["Land_PortableLight_double_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 332.986;
_object setPosATL _pos;
[_object, -1.73831, 0.460264] call BIS_fnc_setPitchBank;

_pos = [10121.365234,15897.896484,0];
_object = createVehicle ["Land_ClutterCutter_large_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;


_pos = [10116.365234,15892.896484,-0.0777359];
_object = createVehicle ["Land_ClutterCutter_large_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;


_pos = [10114.365234,15898.896484,0.116653];
_object = createVehicle ["Land_ClutterCutter_large_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;

_pos = [10120.679688,15888.827148,-0.0714417];
_object = createVehicle ["Land_ClutterCutter_large_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 0;
_object setPosATL _pos;


_pos = [10170.00292969,15902.813477,0.119072];
_object = createVehicle ["Land_Wreck_Hunter_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 112.999;
_object setPosATL _pos;
[_object, 0, -0] call BIS_fnc_setPitchBank;


_pos = [10159.624023,15837.78418,0.0552063];
_object = createVehicle ["Land_Wreck_Ural_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 61.5451;
_object setPosATL _pos;
[_object, -10.0057, -8.95894] call BIS_fnc_setPitchBank;


_pos = [10101.633789,15900.265625,-0.443886];
_object = createVehicle ["Land_HelipadCircle_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 197.468;
_object setPosATL _pos;