/*
	Author: Jimbobob
	Filename: medWep.sqf
	Description: 
	Forces removal of medic's weapons as they're not allowed them!
*/

switch (playerside) do
{
	case civilian:
	{
	};
	
	case west:
	{
	};
	
	case independent: 
	{
		while(true)do
		{
			player action ["DropWeapon", weaponHolder, "currentWeapon player"];
			hint "Medics are not allowed to carry Weapons!";
			sleep 60;
		};
	};
};