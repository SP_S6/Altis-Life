/*
	File: fn_earplugs.sqf
	Author: SP_S6
	Description:
	Add/remove earplugs.
*/
life_earPlugs = !life_earPlugs;

if (life_earPlugs) then 
{
	1 fadeSound 0.1; // lower sound %
	titleText ["You have put your earplugs in.", "PLAIN"];
}
else
{
	1 fadeSound 1; // reset found to full
	titleText ["You have removed your earplugs.", "PLAIN"];
};